from pathlib import Path
CSV_PATH = Path(Path.cwd(), 'files', 'notes.csv')
is_required = {'name': True, 'surname': True, 'phone': True, 'address': False, 'date of birth': False}
