import csv
import os

from objects.note import Note
from datetime import datetime

from const import CSV_PATH


class FileManager:

    def __init__(self):
        self.path_to_file = CSV_PATH
        self.headers = ['id', 'name', 'surname', 'address', 'phone', 'birthday']

    def read_from_file(self):
        if os.path.isfile(self.path_to_file):
            notes = []
            rows = []
            with open(self.path_to_file, 'r', newline='') as csv_file:
                dict_reader = csv.DictReader(csv_file)
                for r in dict_reader:
                    rows.append(r)
                    notes.append(Note(int(r['id']), r['name'], r['surname'], r['address'], r['phone'],
                                      datetime.strptime(r['birthday'], '%d-%m-%Y')))
            return notes
        else:
            with open(self.path_to_file, 'w', newline='') as csv_file:
                dict_writer = csv.DictWriter(csv_file, self.headers)
                dict_writer.writeheader()
        return []

    def write_to_file(self, notes):
        with open(self.path_to_file, 'w', newline='') as csv_file:
            dict_writer = csv.DictWriter(csv_file, self.headers)
            dict_writer.writeheader()
            for note in notes:
                dict_note = {'id': note.id, 'name': note.name, 'surname': note.surname,
                             'address': note.address, 'phone': note.phone,
                             'birthday': note.birthday.strftime('%d-%m-%Y')}
                dict_writer.writerow(dict_note)

    def append_to_file(self, note):
        with open(self.path_to_file, 'a', newline='') as csv_file:
            dict_writer = csv.DictWriter(csv_file, self.headers)
            dict_writer.writerow({'id': note.id, 'name': note.name, 'surname': note.surname,
                                  'address': note.address, 'phone': note.phone,
                                  'birthday': note.birthday.strftime('%d-%m-%Y')})
