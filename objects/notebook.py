import re
from const import is_required
from datetime import datetime
from objects.filemanager import FileManager
from objects.note import Note
from tabulate import tabulate


class Notebook:

    def __init__(self):
        self.file_manager = FileManager()
        self.notes = self.file_manager.read_from_file()
        print(f'{len(self.notes)} notes download from file') if self.notes else print('File is empty '
                                                                                      'or file does not exist')

    def show_notes(self):
        self.tabulate_notes(self.notes) if self.notes else print('Notebook is empty.')

    def add_note(self):
        name = self.is_valid('name')
        surname = self.is_valid('surname')
        phone = self.is_valid('phone')
        address = input("Input address: ")
        date_of_birth = self.is_valid('date of birth')
        idn = (self.notes[-1].id + 1) if self.notes else 0
        note = Note(idn, name, surname, address, phone, date_of_birth)
        self.notes.append(note)
        self.file_manager.append_to_file(note)
        print(f'{note} - note is added to notebook')

    def del_note(self):
        try:
            is_find = False
            del_id = int(input("Input id note for delete: "))
            for item in self.notes:
                if item.id == del_id:
                    is_find = True
                    self.notes.remove(item)
                    print(f'Note with id {del_id} is deleted')
                    self.file_manager.write_to_file(self.notes)
            if not is_find:
                print(f'Note with id {del_id} not found')
        except ValueError:
            print('Input correct id note.')
            self.del_note()

    def find_notes(self):
        try:
            choice = int(input('1 - Find by name\n'
                               '2 - Find by phone\n'
                               'YOUR CHOICE: '))
            searching_notes = []
            match choice:
                case 1:
                    name = input("Input a search name: ")
                    searching_notes = [note for note in self.notes if note.name.lower().startswith(name.lower())]
                case 2:
                    phone = input("Input a search phone: ")
                    searching_notes = [note for note in self.notes if note.phone.startswith(phone)]
            self.tabulate_notes(searching_notes) if searching_notes else print('No matches found')
        except ValueError:
            print('Please, input correct choice')
            self.find_notes()

    def sort(self):
        try:
            choice = int(input('1 - Sort notes by name\n'
                               '2 - Sort notes by surame\n'
                               'YOUR CHOICE: '))
            sorted_notes = []
            match choice:
                case 1:
                    sorted_notes = sorted(self.notes, key=lambda note: note.name.lower())
                case 2:
                    sorted_notes = sorted(self.notes, key=lambda note: note.surname.lower())
            self.tabulate_notes(sorted_notes)
        except ValueError:
            print('Please, input correct choice')
            self.sort()

    def edit_note(self):
        try:
            is_find = False
            edit_id = int(input("Input id note for editing: "))
            for item in self.notes:
                if item.id == edit_id:
                    is_find = True
                    while True:
                        edit_field = int(input(f"EDITING MODE. NOTE ID {item.id}".center(50) + '\n' + f'{item}' +
                                               "\n1-Name\n"
                                               "2-Surname\n"
                                               "3-Address\n"
                                               "4-Phone\n"
                                               "5-Date of birth\n"
                                               "0-EXIT FROM EDITING MODE\n"
                                               "YOUR CHOICE: "))
                        match edit_field:
                            case 1:
                                item.name = self.is_valid('name')
                                continue
                            case 2:
                                item.surname = self.is_valid('surname')
                                continue
                            case 3:
                                item.address = self.is_valid('address')
                                continue
                            case 4:
                                item.phone = self.is_valid('phone')
                                continue
                            case 5:
                                item.birthday = self.is_valid('date of birth')
                                continue
                            case 0:
                                break
                    self.file_manager.write_to_file(self.notes)
            if not is_find:
                print(f'Note with id {edit_id} not found')
        except ValueError:
            print('Input correct id note.')
            self.edit_note()

    def exit(self):
        self.file_manager.write_to_file(self.notes)
        exit(0)

    @staticmethod
    def tabulate_notes(notes):
        dict_notes = [{'id': note.id, 'name': note.name, 'surname': note.surname,
                       'address': note.address, 'phone': note.phone,
                       'birthday': note.birthday.strftime('%d-%m-%Y')} for note in notes]
        print(tabulate(dict_notes, headers="keys", tablefmt="heavy_grid"))

    @staticmethod
    def is_valid(item_name):
        valid = False
        while not valid:
            item = input(f"Input {item_name}{'*' if is_required[item_name] else ''}:")
            match item_name:
                case 'name':
                    if len(item) < 2:
                        print(f'Input correct {item_name}')
                        continue
                case 'surname':
                    if len(item) < 2:
                        print(f'Input correct {item_name}')
                        continue
                case 'phone':
                    if len(item) == 0 or re.match(r'^\d{10}$', item) is None:
                        print(f'Input correct {item_name}')
                        continue
                case 'date of birth':
                    try:
                        return datetime.strptime(item, '%d-%m-%Y')
                    except ValueError:
                        print(f'Input correct {item_name} in format dd-mm-yyyy')
                        continue
            return item
