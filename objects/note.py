class Note:

    def __init__(self, idn, name, surname, address, phone, birthday):
        self.id = idn
        self.name = name
        self.surname = surname
        self.phone = phone
        self.address = address
        self.birthday = birthday

    def __str__(self):
        return f'{self.id}-{self.name}-{self.surname}-{self.phone}-{self.address}-({self.birthday.strftime("%d-%m-%Y")})'

