from objects.notebook import Notebook


def main(nb):
    while True:
        try:
            choice = int(input("1-SHOW NOTES\n"
                               "2-ADD NOTE\n"
                               "3-DELETE NOTE\n"
                               "4-EDIT NOTE\n"
                               "5-FIND NOTE\n"
                               "6-SORT NOTE\n"
                               "0-EXIT\n"
                               "YOUR CHOICE: "))
        except ValueError:
            print("Input correct choice.")
            continue

        match choice:
            case 1:
                nb.show_notes()
            case 2:
                nb.add_note()
            case 3:
                nb.del_note()
            case 4:
                nb.edit_note()
            case 5:
                nb.find_notes()
            case 6:
                nb.sort()
            case 0:
                nb.exit()
            case _:
                print("Input correct choice.")


if __name__ == "__main__":
    notebook = Notebook()
    main(notebook)
